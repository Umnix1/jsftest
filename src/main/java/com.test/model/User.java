package com.test.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="users")
public class User {

  @Id
  @GeneratedValue(strategy= GenerationType.SEQUENCE)
  private Integer id;

  private String name;
  private Boolean isMusician;
  private Integer salary;

  public User() {  }

  public User(Integer id, String name, Boolean isMusician, Integer salary) {
    this.id = id;
    this.name = name;
    this.isMusician = isMusician;
    this.salary = salary;
  }

  public Integer getId() {
    return id;
  }

  public void setId(Integer id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Boolean getIsMusician() {
    return isMusician;
  }

  public void setIsMusician(Boolean musician) {
    isMusician = musician;
  }

  public Integer getSalary() {
    return salary;
  }

  public void setSalary(Integer salary) {
    this.salary = salary;
  }
}
