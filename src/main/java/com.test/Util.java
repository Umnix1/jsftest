package com.test;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class Util {

  private static EntityManagerFactory emf = null;

  public static EntityManager getEntityManager() {
    if (emf == null) {
      emf = Persistence.createEntityManagerFactory("jsfPU");
    }
    return emf.createEntityManager();
  }
}
