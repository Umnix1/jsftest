package com.test.beans;

import com.test.Util;
import com.test.model.User;
import java.io.Serializable;
import java.util.List;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.persistence.EntityManager;

@ManagedBean(name="userbean")
@SessionScoped
public class UserBean implements Serializable {

  private static final long serialVersionUID = 1L;

  private static List<User> userList;

  public List<User> persistUsers() {
      EntityManager entityManager = Util.getEntityManager();
      entityManager.getTransaction().begin();

      userList = entityManager.createQuery(
          "select users from User users", User.class).getResultList();

      entityManager.getTransaction().commit();
      entityManager.close();

      if (userList.isEmpty())
        System.out.println("Empty List");
      return userList;
    }


  public static void setUserList(List<User> userList) {
    UserBean.userList = userList;
  }

  public List<User> getUserList() {
    return userList;
  }

}
